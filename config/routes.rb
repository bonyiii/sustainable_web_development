# frozen_string_literal: true

Rails.application.routes.draw do
  resources :widgets, only: %i[show index new create]
  resources :widget_ratings, only: [:create]

  # Defines the root path route ("/")
  # root "articles#index"
  get 'manufacturer/:id', to: 'manufacturers#show'

  if Rails.env.development?
    resources :design_system_docs, only: [:index]
  end

  ####
  # Custom routes start here
  #
  # For each new custom route:
  #
  # * Be sure you have the canonical route declared above
  # * Add the new custom route bloew the exisiting ones
  # * Document why it's needed
  # * Explain anything else non-standard

  # Used in podcast ads for the 'amazing' campaign
  get '/amazing', to: redirect('/widgets')
end

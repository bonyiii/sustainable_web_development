require "test_helper"

class WidgetCreatorTest < ActiveSupport::TestCase
  setup do
    @widget_creator = WidgetCreator.new
    @manufacturer = manufacturers(:pear)
    ActionMailer::Base.deliveries = []
  end

  test "widgets have a default status of 'Fresh'" do
    result = @widget_creator.create_widget(Widget.new(
                                             name: "Stembolt 2",
                                             price_cents: 1_000_00,
                                             manufacturer_id: @manufacturer.id
                                           ))
    assert result.created?
    assert_equal Widget.last, result.widget
    assert_equal "Fresh", result.widget.widget_status.name
    assert_equal 0, ActionMailer::Base.deliveries.size
  end

  test "widget names must be 5 characters or greater" do
    result = @widget_creator.create_widget(Widget.new(
                                             name: "widg",
                                             price_cents: 1_000_00,
                                             manufacturer_id: @manufacturer.id
                                           ))

    refute result.created?
    assert result.widget.invalid?

    too_short_error = result.widget.errors[:name].detect { |message|
      message =~ /is too short/i
    }

    refute_nil too_short_error, result.widget.errors.full_messages.join(", ")
  end

  test "finance is notified for widgets priced over $7,500" do
    result = @widget_creator.create_widget(
      Widget.new(
        name: "Another widget",
        price_cents: 7_500_01,
        manufacturer_id: @manufacturer.id
      ))

    assert result.created?
    assert_equal 1, ActionMailer::Base.deliveries.size
    mail_message = ActionMailer::Base.deliveries.first
    assert_equal "finance@example.org", mail_message["to"].to_s
    assert_match (/Another widget/), mail_message.text_part.to_s
  end

  test "name, price and manufacturer are required" do
    result = @widget_creator.create_widget(Widget.new)

    refute result.created?

    widget = result.widget
    assert widget.invalid?

    assert widget.errors[:name].any? { |message|
      message =~ /can't be blank/i
    }, widget.errors.full_messages_for(:name)

    assert widget.errors[:price_cents].any? { |message|
      message =~ /is not a number/i
    }, widget.errors.full_messages_for(:price_cents)

    assert widget.errors[:manufacturer].any? { |message|
      message =~ /must exist/i
    }, widget.errors.full_messages_for(:manufacturer)
  end

  test "price cannot be 0" do
    result = @widget_creator.create_widget(
      Widget.new(
        name: "My Stembolt",
        price_cents: 0,
        manufacturer_id: @manufacturer.id
      ))

    refute result.created?

    assert result.widget.errors[:price_cents].any? { |message|
      message =~ /greater than 0/i
    }, result.widget.errors.full_messages_for(:price_cents)
  end

  test "price cannot be more than 10,000" do
    result = @widget_creator.create_widget(
      Widget.new(
        name: "My Stembolt",
        price_cents: 10_000_01,
        manufacturer_id: @manufacturer.id
      ))

    refute result.created?

    assert result.widget.errors[:price_cents].any? { |message|
      message =~ /less than or equal to 100000/i
    }, result.widget.errors.full_messages_for(:price_cents)
  end

  test "legacy manufacturers cannot have a price under $100" do
    @manufacturer.update_column(:created_at, DateTime.new(2010,1,1) - 1.day)
    @manufacturer.reload
    result = @widget_creator.create_widget(
      Widget.new(
        name: "My Stembolt",
        price_cents: 90_01,
        manufacturer_id: @manufacturer.id
      ))

    refute result.created?

    assert result.widget.errors[:price_cents].any? { |message|
      message =~ /< \$100.*legacy/i
    }, result.widget.errors.full_messages_for(:price_cents)
  end

  test "email admin staff for widgets on new manufacturers" do
    @manufacturer.update_column(:created_at, 59.days.ago)
    @manufacturer.reload
    result = @widget_creator.create_widget(
      Widget.new(
        name: "My Stembolt",
        price_cents: 8_000_01,
        manufacturer_id: @manufacturer.id
      ))

    assert result.created?
    assert_equal 2, ActionMailer::Base.deliveries.size
    mail_message = ActionMailer::Base.deliveries.first
    assert_equal "finance@example.org", mail_message["to"].to_s
    assert_match (/My Stembolt/), mail_message.text_part.to_s
  end
end

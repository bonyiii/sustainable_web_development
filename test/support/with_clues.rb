module TestSupport
  module WithClues
    # Wrap any assertion with this method to get more
    # useful context and diagnostics when a test is
    # unexpectedly failing
    def with_clues(&block)
      block.()
    rescue Exception => ex
      puts "[ with clues ] Test failed: #{ex.message}"
      puts
      puts "[ with clues ] HTML {"
      puts page.html
      puts
      puts "[ with clues ] } END HTML"
      raise ex
    end
  end
end

#gem 'fixture_builder'
#require 'fixture_builder'

FixtureBuilder.configure do |fbuilder|
  fbuilder.files_to_check += Dir[
    'db/structure.sql',
    'test/fixtures/*.rb'
  ]

  fbuilder.factory do
    fixtures_path = %w(
    addresses
    manufacturers
    widget_statuses
    widgets
    ).map do |name|
      Rails.root.join("test", "fixtures", "#{name}.rb")
    end

    fixtures_path.each do |path|
      File.open(path) { |file| eval(file.read) }
    end
  end
end

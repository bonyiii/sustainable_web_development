require "test_helper"

Capybara.register_driver :root_headless_chrome do |app|
  capabilities = Selenium::WebDriver::Options.chrome(
    "goog:chromeOptions": {
                            args: [
                              "headless",
                              "disable-gpu",
                              "no-sandbox",
                              "disable-dev-shm-usage",
                              "whitelisted-ips"
                            ]
                          },
    "goog:loggingPrefs": { browser: "ALL"}
  )

  # This is temporarily fix for Webdriver.logger, it seems it needs to be invoked
  # before test runs, likely because of lazy loading order issues
  # See related github thread: 
  # https://github.com/teamcapybara/capybara/issues/2666#issuecomment-1540496771
  Selenium::WebDriver.logger

  Capybara::Selenium::Driver.new(
    app,
    browser: :chrome,
    options: capabilities
  )

  #Webdrivers::Chromedriver.required_version =
end # register_driver

require "support/with_clues"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  include TestSupport::WithClues
  # driven_by :selenium, using: :chrome, screen_size: [1400, 1400]
  # We primarily focus on server side rendering idea is
  # features should run without javascript
  # when needed we will turn on chrome driver
  driven_by :rack_test
end

class BrowserSystemTestCase < ApplicationSystemTestCase
  driven_by :root_headless_chrome, screen_size: [1400, 1400]
end

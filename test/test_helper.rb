ENV["RAILS_ENV"] ||= "test"

require_relative "../config/environment"
require "rails/test_help"

# In order to make sure bundler does it's job
# eg: setup gems we need, let config/environment
# run first, it call: Rails.application.initialize!
#
# If we require support/fixture_builder before rails initialize bundler
# we might see issues like: uninitialized constant FixtureBuilder (NameError)
require_relative 'support/fixture_builder'

Capybara.configure do |config|
  # This allows helpers like click_on to locate
  # any objects by data-testid in addtion to
  # built in selector-like values
  config.test_id = "data-testid"
end

require 'support/confidence_check'

class ActiveSupport::TestCase
  include TestSupport::ConfidenceCheck
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end

// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"

// The default 500ms is too long and
// users can loose the casual link between clicking
// a link and seeing the browser respond
Turbo.setProgressBarDelay(100)

import "controllers"

import { WidgetRatings } from "widget_ratings"
WidgetRatings.start(window)

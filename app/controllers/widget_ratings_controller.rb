# frozen_string_literal: true

# Controller to handle widget ratings
class WidgetRatingsController < ApplicationController
  def create
    if params[:widget_id]
    # find the widget
    # update its ratings
    # redirect_to widget_path(params[:widget_id]), notice: "Thanks for rating!"

    # default rails behaviour is to render if no redirect or other code
    # tells otherwise happens
    else
      head :bad_request
    end
  end
end

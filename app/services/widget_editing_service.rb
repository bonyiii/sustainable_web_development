class WidgetEditingService
  def edit_widget|(widget, widget_params)
    widget.update(widget_params)

    if widget.valid?
      EditWidgetJob.perform_later(widget.id)
    end

    widget
  end

  def post_widget_edit(widget)
    # create InventtoryReport
    # check manufacturer to see who to notify
    # trigger AdminMailer to notify the right person
  end
end
